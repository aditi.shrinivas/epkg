# Pathway:
# Get a directory to archive
# make a tarfile object
# Traverse the directory and read all the files into the tarfile
# Prepare a msgpack header to the file
# Encrypt the tarfile string
# Put it all together
import msgpack


def __init__(hub):
    # Remember not to start your app in the __init__ function
    # This function should just be used to set up the plugin subsystem
    # The run.py is where your app should usually start
    for dyne in []:
        hub.pop.sub.add(dyne_name=dyne)
    hub.pop.sub.load_subdirs(hub.epkg)
    hub.epkg.DELIM = '4bb1a557-f0ff-41c5-83b0-420e8c5f71be'


def cli(hub):
    hub.pop.config.load(['epkg'], cli="epkg")
    # Your app's options can now be found under hub.OPT.epkg
    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Start the async code
    coroutine = hub.epkg.init.run()
    hub.pop.Loop.run_until_complete(coroutine)


async def run(hub):
    """
    This is the entrypoint for the async code in your project
    """
    if hub.SUBPARSER == "create":
        hub.epkg.init.create()
    elif hub.SUBPARSER == "display":
        hub.epkg.display.run()

def create(hub):
    hub.epkg.ar.init.prepare()
    hub.epkg.enc.init.run()
    hub.epkg.init.save()
    print(f'Encrypted package created at: {hub.OPT.epkg.output}')
    key = hub.epkg.KEY.decode()
    print(f'Package generated with key: {key}')


def save(hub):
    """
    Prepare the data that defines the encryption and archive type used
    """
    data = {"archive": hub.epkg.AR,
            "compression": hub.epkg.COMPRESSION,
            "encryption": hub.epkg.ENC,
            "token": hub.epkg.TOKEN,
            }
    out = hub.OPT.epkg.output
    if not out.endswith(".epkg"):
        out += ".epkg"
    with open(out, 'wb+') as wfh:
        wfh.write(msgpack.dumps(data))
