import msgpack


def run(hub):
    with open(hub.OPT.epkg.pkg, "rb") as rfh:
        data = msgpack.loads(rfh.read())
    ar = data["archive"]
    compression = data["compression"]
    enc = data["encryption"]
    token = data["token"]
    bio = hub.epkg.enc[enc].decrypt(token, hub.OPT.epkg.key)
    hub.epkg.ar[ar].list(bio)
