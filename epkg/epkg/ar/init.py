def prepare(hub):
    """
    Prepare the archive file
    """
    hub.epkg.ar[hub.OPT.epkg.ar_format].prepare()


def add(hub, path):
    """
    Add the given path to the archive
    """
    hub.epkg.ar[hub.OPT.epkg.ar_format].add(path)
