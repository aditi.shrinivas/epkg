import io
import os
import tarfile


def prepare(hub):
    hub.epkg.AR = "tar"
    hub.epkg.COMPRESSION = "xz"
    hub.epkg.BYTES = io.BytesIO()
    with tarfile.open(fileobj=hub.epkg.BYTES, mode="w:xz") as tar:
        tar.add(hub.OPT.epkg.dir, arcname=os.path.basename(hub.OPT.epkg.dir))
    hub.epkg.BYTES.flush()
    hub.epkg.BYTES.seek(0)


def list(hub, bio):
    raw = io.BytesIO(bio)
    with tarfile.open(fileobj=raw, mode="r:xz") as tar:
        tar.list()
