# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data

# In this dictionary goes all the immutable values you want to show up under hub.OPT.epkg
CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.epkg",
    },
    "ar_format": {
        "default": "tar",
        "help": "The archive format to use for the saved files",
        },
    "enc": {
        "default": "fernet",
        "help": "The encryption type to use for the data",
        },
    "dir": {
        "default": ".",
        "help": "The directory to make into an encrypted archive",
        },
    "output": {
        "default": "out.epkg",
        "help": "The location of the saved, encrypted, archive package",
        },
    "pkg": {
        "default": None,
        "help": "The name of the package to extract, it will be extracted to the present working directory",
        },
    "key": {
        "default": None,
        "help": "The key to use to decrypt the given package",
        },
}

# The selected subcommand for your cli tool will show up under hub.SUBPARSER
# The value for a subcommand is a dictionary that will be passed as kwargs to argparse.ArgumentParser.add_subparsers
SUBCOMMANDS = {
    # "my_sub_command": {}
    "create": {
        "desc": "Create an epkg",
        "help": "Used to create epkgs",
        },
    "display": {
        "desc": "Display the contents of an epkg",
        "help": "Display the contents of an epkg",
        },
}

# Include keys from the CONFIG dictionary that you want to expose on the cli
# The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
CLI_CONFIG = {
    "config": {"options": ["-c"]},
    "ar_format": {
        "subcommands": ["create"],
        },
    "enc": {
        "subcommands": ["create"],
        },
    "dir": {
        "subcommands": ["create"],
        },
    "output": {
        "subcommands": ["create"],
        },
    "pkg": {
        "display_priority": 0,
        "positional": True,
        "subcommands": ["display"],
        },
    "key": {
        "display_priority": 1,
        "positional": True,
        "subcommands": ["display"],
        },
    # "my_option1": {"subcommands": ["A list of subcommands that exclusively extend this option"]},
    # This option will be available under all subcommands and the root command
    # "my_option2": {"subcommands": ["_global_"]},
}

# These are the namespaces that your project extends
# The hub will extend these keys with the modules listed in the values
DYNE = {'epkg': ['epkg']}
